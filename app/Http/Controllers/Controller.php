<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    private $response = [
        'success' => true,
        'error' => null,
        'data' => []
    ];

    public function __construct()
    {
    }

    protected function response($data)
    {
        if (isset($GLOBALS['request']) && $GLOBALS['request']->expectsJson()) {
            $this->response['data'] = $data;
            return response()->json($this->response);
        }

        return $data;
    }


    /**
     * @param string $message
     * @param int $errorCode
     * @param array|\Exception $fields
     * @param int $status
     * @return \Illuminate\Http\JsonResponse
     */
    protected function errorResponse($message, $errorCode = 0, $fields = null, $status = 400)
    {
        $this->response['success'] = false;
        unset($this->response['data']);

        if ($fields instanceof \Exception) {
            $fields = [
                'exception_code' => [$fields->getCode()],
                'exception_message' => [$fields->getMessage()],
                'exception_file' => [$fields->getFile()],
                'exception_line' => [$fields->getLine()],
            ];
        }


        $this->response['error'] = [
            'message' => $message,
            'code' => $errorCode,
            'fields' => $fields
        ];

        return response()->json($this->response, $status);
    }

}
