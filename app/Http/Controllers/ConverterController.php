<?php

namespace App\Http\Controllers;

use App\Jobs\ConvertQueued;
use Illuminate\Http\Request;
use Validator;

class ConverterController extends Controller
{
    public function convert(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'model' => 'required',
            'userId' => 'required',
        ]);

        if ($validator->fails()) {
            return;
        }

            $path = $request->get('model');
            $userId = $request->get('userId');

            dispatch(new ConvertQueued($userId, $path));

    }
}
