<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ConvertQueued implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $modelPath;
    private $userId;

    /**
     * Create a new job instance.
     *
     * @param int $userId
     * @param string $modelPath
     *
     * @return void
     */
    public function __construct($userId, $modelPath)
    {
        $this->modelPath = $modelPath;
        $this->userId = $userId;
    }


    public function tags()
    {
        return [
            'convert: ' . $this->modelPath,
            'userId: ' . $this->userId,
        ];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $fullPath = \Storage::disk('upload')->path($this->userId . '/raw/' . $this->modelPath);
        exec(app_path('../converter.sh' . ' ' . $fullPath));
    }
}
