<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    public $timestamps = false;
    protected $guarded = ['id'];

    static public function getSettingValue($key, $default = null)
    {
        if ($setting = self::getSetting($key)) {
            return $setting->value;
        }

        return $default;
    }

    static public function getSetting($key)
    {
        $setting = self::where('key', strtoupper($key))->get();
        if ($setting->count()) {
            return $setting->first();
        }

        return null;
    }

}
