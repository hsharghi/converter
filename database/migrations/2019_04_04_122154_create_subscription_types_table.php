<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscription_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->mediumInteger('storage')->nullable();
            $table->mediumInteger('num_public_models')->nullable();
            $table->mediumInteger('num_private_models')->nullable();
            $table->mediumInteger('max_upload_size')->nullable();
            $table->smallInteger('max_conversions_per_day')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscription_types');
    }
}
