<?php

use Illuminate\Database\Seeder;

class SubscriptionPlansSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $typeId = DB::table('subscription_types')->insertGetId([
                                                    'title' => 'Base',
                                                    'storage' => '20',
                                                    'num_public_models' => null,
                                                    'num_private_models' => '0',
                                                    'max_upload_size' => '10',
                                                    'max_conversions_per_day' => '1',
                                                    'created_at' => \Carbon\Carbon::now(),
                                                    'updated_at' => \Carbon\Carbon::now()
                                                ]);
        DB::table('subscription_plans')->insert([
                                                    'title' => 'Base',
                                                    'subscription_type_id' => $typeId,
                                                    'duration' => null,
                                                    'price' => 0,
                                                ]);
    }
}
