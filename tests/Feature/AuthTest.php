<?php

namespace Tests\Feature;

use App\Models\User\User;
use Tests\TestCase;

class AuthTest extends TestCase
{
    /**
     * @test
     * Test registration
     */
    public function testRegister(){

        $response = $this->registerUser('test');

        //Assert it was successful
        $response->assertStatus(200);

        // Assert user has been created
        $user = $this->getUser('test');
        $this->assertNotNull($user);

        // Assert user has referral code
        $this->assertNotNull($user->referralCode);

        //Assert we received a token
        $this->assertArrayHasKey('token',$response->json());

        //Delete data
        $this->deleteUser('test');
    }

    /**
     * @test
     * Test registration with referral code
     */
    public function testRegisterWithReferralCode(){

        $this->registerUser('user1');
        $user1 = $this->getUser('user1');
        $referralCode = $user1->referralCode;

        $this->registerUser('user2', $referralCode);
        $user2 = $this->getUser('user2');

        // Assert user1 is the referrer of user2
        $this->assertEquals($user2->info->referrer_id, $user1->id);

        //Delete data
        $this->deleteUser('user1');
        $this->deleteUser('user2');
    }



    /**
     * @test
     * Test login
     */
    public function testLogin()
    {
        //Create user
        User::create([
                         'name' => 'test',
                         'email'=>'test@test.com',
                         'password' => bcrypt('secret1234')
                     ]);
        //attempt login
        $response = $this->json('POST',route('api.login'),[
            'email' => 'test@test.com',
            'password' => 'secret1234',
        ]);
        //Assert it was successful and a token was received
        $response->assertStatus(200);
        $this->assertArrayHasKey('token',$response->json());
        //Delete the user
        User::where('email','test@test.com')->delete();
    }

}
