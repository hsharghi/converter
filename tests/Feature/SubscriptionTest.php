<?php

namespace Tests\Feature;

use App\Models\Subscription\SubscriptionPlan;
use App\Models\Subscription\SubscriptionType;
use App\Models\User\User;
use App\Repositories\UserRepository;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SubscriptionTest extends TestCase
{
    /**
     * test default subscription type exists in db
     *
     * @return void
     */
    public function testDefaultSubscriptionTypeExists()
    {
        $plan = SubscriptionType::query()->orderBy('id')->first();
        $this->assertNotNull($plan);
    }


    /**
     * test default subscription plan exists in db
     *
     * @return void
     */
    public function testDefaultSubscriptionPlanExists()
    {
        $type = SubscriptionType::query()->orderBy('id')->first();
        $plan = SubscriptionPlan::where('subscription_type_id', $type->id)->first();
        $this->assertNotNull($plan);
    }


    /**
     * test user has default subscription upon registration
     *
     * @return void
     */
    public function testDefaultSubscriptionOnRegister()
    {
        $this->registerUser('test');
        $user = $this->getUser('test');

        $this->assertEquals($user->subscription()->count(), 1);

        $this->deleteUser('test');

    }

    /**
     * test set user subscription with 7 days duration
     */
    public function testSubscriptionPlanWithDuration()
    {
        $plan = SubscriptionType::query()->orderBy('id')->first();
        $now = Carbon::now();
        $this->registerUser('test');
        $user = $this->getUser('test');

        $repo = new UserRepository($user);
        $repo->assignSubscriptionPlan($plan, \DateInterval::createFromDateString('7 days'));

        $endDate = $user->subscription->end_date;
        $this->assertNotNull($endDate);

        $diff = $now->diffInDays($endDate);

        $this->assertEquals($diff, 7);

        $this->deleteUser('test');

    }


}
